#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <stdint.h>
#include "settings.h"

void blink(uint8_t n) {
  for(uint8_t i = 0; i < n; i++) {
    _delay_ms(BLINK_PERIOD);
    PORT |= (1<<LED);
    _delay_ms(BLINK_PERIOD);
    PORT &= ~(1<<LED);
  }
}

ISR(TIMER0_OVF_vect) {
  cli();
  blink(5);
  sei();
}

ISR(INT0_vect) {
  cli();
  blink(2);
  sei();
}

void setup(void) {
  cli();

  // Setup LED
  DDRC = (1<<LED);

  // Setup interupt pin
  DDRD &= ~(1<<PIN);
  PORTD = (1<<PIN);

  GICR = (1<<INT0);
  MCUCR = (1<<ISC01) | (1<<ISC00);

  // Setup timer
  TCCR1B = (1<<CS12) | (1<<CS10);
  TIMSK |= (1<<TOIE1);

  // Enable interupts
  sei();

  // Setup done indicatior
  blink(5);
}

void loop(void) {
}

int main(void) {
  setup();

  while(1) {
    loop();
  }
}
