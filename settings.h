#pragma once

#define LED 2
#define PIN PD2
#define PORT PORTC
#define DDR DDRC
#define BLINK_PERIOD 500
